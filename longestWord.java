import java.io.*;
import java.util.*;

public class longestWord {

	private Hashtable _hashtable;
	private LinkedList<String> _list;

	public longestWord() throws IOException{
		_hashtable = new Hashtable();
		_list = new LinkedList<String>();
		String dictword = null;
		BufferedReader br = new BufferedReader(new FileReader("/usr/share/dict/words"));
		while((dictword = br.readLine()) != null){
			int length = dictword.length();
			char[] orderedword = dictword.toCharArray();
			Arrays.sort(orderedword);
			String sorted = new String(orderedword);
			_hashtable.put(sorted, dictword);
			_list.add(sorted);
		}
	}

	public String findLongestWord(String word){
		char[] wordinarray = word.toCharArray();
		Arrays.sort(wordinarray);
		String givenWord = new String(wordinarray);
		String longest = null;
		int longestLength = 0;

		while(_list.isEmpty() == false){
			String wordtocheck = _list.remove(0);
			int length = wordtocheck.length();
			for(int i = 0; i<length; i++){
				if(this.check(givenWord, wordtocheck)==true){
					if(longestLength < length){
						longest = wordtocheck;
						longestLength = length;
					}
				}
			}
		}
		String result = (String) _hashtable.get(longest);
		System.out.println(result);
		return result;
	}

	public boolean check(String given, String check){
		int lcheck = check.length();
		int givenCounter = 0;
		int checkCounter = 0;
		while(givenCounter < 16){
			if(checkCounter == lcheck){
				return true;
			}
			if(given.charAt(givenCounter) > check.charAt(checkCounter)){
				return false;
			}
			else if(given.charAt(givenCounter) < check.charAt(checkCounter)){
				givenCounter += 1;
			}
			else if(given.charAt(givenCounter) == check.charAt(checkCounter)){
				givenCounter += 1;
				checkCounter += 1;
			}
		}
		return false;
	}
}